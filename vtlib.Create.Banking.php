<?php
echo '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">';
echo '<html><head><title>vtlib Module Script</title>';
echo '<style type="text/css">@import url("themes/softed/style.css");br { display: block; margin: 2px; }</style>';
echo '</head><body class=small style="font-size: 12px; margin: 2px; padding: 2px;">';
echo '<a href="index.php"><img src="themes/softed/images/vtiger-crm.gif" alt="vtiger CRM" title="vtiger CRM" border=0></a><hr style="height: 1px">'; 

$Vtiger_Utils_Log = true;
include_once('vtlib/Vtiger/Menu.php');
include_once('vtlib/Vtiger/Module.php');

$module = new Vtiger_Module();
$module->name = 'Banking';
$module->save();

$module->initTables();
$module->initWebservice();

$menu = Vtiger_Menu::getInstance('Tools');
$menu->addModule($module);

$block1 = new Vtiger_Block();
$block1->label = 'LBL_BANKING_INFORMATION';
$module->addBlock($block1);

/*BLOCK-1-START*/
$field1 = new Vtiger_Field();
$field1->name = 'name';
$field1->label = 'Name';
$field1->table = 'vtiger_banking';
$field1->column = 'name';
$field1->columntype = 'VARCHAR(255)';
$field1->uitype = 2;
$field1->typeofdata = 'V~M';
$block1->addField($field1);
$module->setEntityIdentifier($field1);

$field7 = new Vtiger_Field();
$field7->name = 'ifsc';
$field7->label = 'IFSC';
$field7->table = 'vtiger_banking';
$field7->column = 'related_to';
$field7->columntype = 'INT(11)';
$field7->uitype = 2;
$field7->typeofdata = 'V~M';
$block1->addField($field7);
// $field7->setrelatedmodules(array('Leads','Accounts','Contacts','Potentials','Products','HelpDesk','Campaigns','Quotes','Invoice','SalesOrder','PurchaseOrder','Services',));

$field10 = new Vtiger_Field();
$field10->name = 'acct_name';
$field10->label = 'Account Name';
$field10->table = 'vtiger_banking';
$field10->column = 'acct_name';
$field10->columntype = 'VARCHAR(255)';
$field10->uitype = 2;
$field10->typeofdata = 'V~M';
$block1->addField($field10);

$field11 = new Vtiger_Field();
$field11->name = 'acct_no';
$field11->label = 'Account No';
$field11->table = 'vtiger_banking';
$field11->column = 'acct_no';
$field11->columntype = 'VARCHAR(255)';
$field11->uitype = 2;
$field11->typeofdata = 'V~M';
$block1->addField($field11);


$field2 = new Vtiger_Field();
$field2->name = 'organization';
$field2->label = 'Organization';
$field2->table = 'vtiger_banking';
$field2->column = 'organization';
$field2->uitype = 10;
$field2->typeofdata = 'D~M';
$block1->addField($field2);


/*COMMON FIELDS*/
$field100 = new Vtiger_Field();
$field100->name = 'assigned_user_id';
$field100->label = 'Assigned To';
$field100->table = 'vtiger_crmentity';
$field100->column = 'smownerid';
$field100->uitype = 53;
$field100->typeofdata = 'V~M';
$block1->addField($field100);

$field101 = new Vtiger_Field();
$field101->name = 'createdtime';
$field101->label= 'Created Time';
$field101->table = 'vtiger_crmentity';
$field101->column = 'createdtime';
$field101->uitype = 70;
$field101->typeofdata = 'T~O';
$field101->displaytype= 2;
$block1->addField($field101);

$field102 = new Vtiger_Field();
$field102->name = 'modifiedtime';
$field102->label= 'Modified Time';
$field102->table = 'vtiger_crmentity';
$field102->column = 'modifiedtime';
$field102->uitype = 70;
$field102->typeofdata = 'T~O';
$field102->displaytype= 2;
$block1->addField($field102);
/*COMMON FIELDS*/


/*BLOCK-1-END*/


//Filter
$filter1 = new Vtiger_Filter();
$filter1->name = 'All';
$filter1->isdefault = true;
$module->addFilter($filter1);

// Add fields to the filter created
$filter1->addField($field1)->addField($field2, 1)->addField($field3, 2)->addField($field4, 3)->addField($field5, 4);

//Reated modules
/*$module->setList(Vtiger_Module::getInstance('Products'), 'Products', Array('SELECT'),'get__list');
$module->setList(Vtiger_Module::getInstance('Documents'), 'Documents', Array('ADD','SELECT'),'get_attachments');
$module->setList(Vtiger_Module::getInstance('Accounts'), 'Accounts', Array('SELECT'),'get__list');
$module->setList(Vtiger_Module::getInstance('Contacts'), 'Contacts', Array('SELECT'),'get__list');
$module->setList(Vtiger_Module::getInstance('HelpDesk'), 'HelpDesk', Array('ADD'),'get_dependents_list');*/

//sharing access of this module
$module->setDefaultSharing('Public');

//Enable and Disable available tools
$module->enableTools(Array('Import', 'Export'));
$module->disableTools('Merge');

echo '</body></html>';
?>
